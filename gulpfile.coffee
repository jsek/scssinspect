gulp = require 'gulp'
$    = require('gulp-load-plugins')()
require 'xunit-file'

gulp.task 'compile-lib', ->
    gulp.src 'lib/**/*.coffee'
        .pipe $.coffee bare: true
        .pipe gulp.dest 'dist/lib'
        
gulp.task 'compile-spec', ->
    gulp.src 'spec/**/*.coffee'
        .pipe $.coffee bare: true
        .pipe gulp.dest 'dist/spec'
        
gulp.task 'copy-parser', ->
    gulp.src 'parser/*.*'
        .pipe gulp.dest 'dist/parser'

gulp.task 'test', ->
    gulp.src 'spec/**/*.coffee', read: false
        .pipe $.mocha reporter: 'dot'
        .pipe gulp.dest 'shippable/mocha'

gulp.task 'test-shippable', ->
    gulp.src 'spec/**/*.coffee', read: false
        .pipe $.mocha reporter: 'xunit-file'

gulp.task 'watch', ->
    gulp.watch 'lib/**/*.coffee'    , ['compile-lib']
    gulp.watch 'parser/*.*'         , ['copy-parser']

gulp.task 'build', ['compile-lib', 'copy-parser']
gulp.task 'default', ['build', 'test']